#!/usr/bin/env bash

repository="https://gitlab.com/nicolabelluti/hassio-docker-compose"

# Match the machine types and the Docker repository with the CPU architecture
case $(uname -m) in
	i386|i686)
		docker_image="homeassistant/i386-hassio-supervisor"
		machines=("qemux86")
		;;
	x86_64)
		docker_image="homeassistant/amd64-hassio-supervisor"
		machines=("qemux86-64" "intel-nuc")
		;;
	arm|armv6l)
		docker_image="homeassistant/armhf-hassio-supervisor"
		machines=("qemuarm" "raspberrypi")
		;;
	armv7l)
		docker_image="homeassistant/armv7-hassio-supervisor"
		machines=("odroid-xu" "raspberrypi2" "raspberrypi3" "raspberrypi4" "tinker")
		;;
	aarch64)
		docker_image="homeassistant/aarch64-hassio-supervisor"
		machines=("qemuarm-64" "odroid-c2" "odroid-n2" "raspberrypi3-64" "raspberrypi4-64")
		;;
	*)
		echo "Unknown architecture."
		echo "This installation script supports only i386, i686, x86_64, arm, armv6l, armv7l and aarch64 architectures"
		exit
		;;
esac

# Print all compatible machines
foo=1
for machine in ${machines[*]}; do
	echo $foo\) $machine
	foo=$(($foo+1))
done

# Let the user choose the machine type
while [[ $foo -lt 1 || $foo -gt ${#machines[@]} ]]; do
	read -ep "Select your machine type (${machines[0]}): " foo
	[[ -z $foo ]] && foo=1
done
machine=${machines[$foo-1]}

# Let the user choose the config folder
read -ep "Select the location of the config folder (/home/$(whoami)/.hassio): " directory
[[ -z $directory ]] && directory="/home/$(whoami)/.hassio"
echo "$directory" | grep "^/" &>/dev/null || directory="$(pwd)/$directory"

# Print a recap message
echo ""
echo "Docker image:   $docker_image"
echo "Machine type:   $machine"
echo "Config folder:  $directory"
echo ""

# Download the 'docker-compose.yaml' file from the repository
wget "$repository/raw/master/docker-compose.yaml" -O docker-compose.yaml &> /dev/null

# Substitute the variables
sed -i "s,\${DOCKER_IMAGE},$docker_image," docker-compose.yaml
sed -i "s,\${MACHINE},$machine," docker-compose.yaml
sed -i "s,\${DIRECTORY},$directory," docker-compose.yaml
