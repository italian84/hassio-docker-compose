# Hass.io - Docker Compose

Install Hass.io with Docker Compose

**This installation method is unsupported.** Follow this guide only if you know what you're doing

Use this method only if you don't want to install additional packages and if you want to take control of your Docker instance

### Index

1. [Installation Script](#1-installation-script)

2. [Manual Installation](#2-manual-installation)

3. [Problems](#3-problems)

## 1. Installation Script

If you don't want to edit manually the `docker-compose.yaml` you can download the `create-docker-compose.sh` file and use it to automatically download and edit the configuration file

Copy and paste in the terminal:

``` bash
wget https://gitlab.com/nicolabelluti/hassio-docker-compose/raw/master/create-docker-compose.sh
chmod +x create-docker-compose.sh
./create-docker-compose.sh
```

## 2. Manual Installation

If you want to take control over your installation, you have to manually download and edit the `docker-compose.yaml` file in order to change the `${DOCKER_IMAGE}` variable with the absolute path of a directory in which the configuration files will go

You have also to customize the `${DIRECTORY}` and `${MACHINE}` variables

Follow this table to find out which values to use:

| Architecture (output of `uname -m`) | Docker Image                              | Machine (select one)                                                              |
|:-------------------------:|:-----------------------------------------:|:------------------------------------------------------------------------:|
| `i386` or `i686`          | `homeassistant/i386-hassio-supervisor`    | `qemux86`                                                                |
| `x86_64`                  | `homeassistant/amd64-hassio-supervisor`   | `qemux86-64` `intel-nuc`                                                 |
| `arm` or `armv6l`         | `homeassistant/armhf-hassio-supervisor`   | `qemuarm` `raspberrypi`                                                  |
| `armv7l`                  | `homeassistant/armv7-hassio-supervisor`   | `odroid-xu` `raspberrypi2` `raspberrypi3` `raspberrypi4` `tinker`        |
| `aarch64`                 | `homeassistant/aarch64-hassio-supervisor` | `qemuarm-64` `odroid-c2` `odroid-n2` `raspberrypi3-64` `raspberrypi4-64` |

## 3. Problems

* AppArmor is not supported

* You cannot upgrade Home Assistant OS, since is not used

* You'll get an error regarding `rauc` at startup (related to the point above)

* All the containers, except `hassio_supervisor`, are dynamically generated. This means that Docker Compose cannot manage them (for example, if one suddenly stops, you have to turn it on manually)
